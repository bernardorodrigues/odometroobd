import obd
import datetime

def read_speed(connection):
    response = connection.query(obd.commands.RPM)
    response = connection.query(obd.commands.SPEED)

    while response.value == None:
        response = connection.query(obd.commands.RPM)
        response = connection.query(obd.commands.SPEED)

    return float(response.value.magnitude)

connection = obd.OBD() # auto-connects to USB
last_v = read_speed(connection)
last_t = datetime.datetime.now()

f = open('data', 'w')

while True:
    v = read_speed(connection)
    t = datetime.datetime.now()

    delta_t = (t - last_t).total_seconds()
    delta_v = abs(v - last_v)/3.6      #m/s
    a = delta_v/delta_t

    while a > 4:
        v = read_speed(connection)
        t = datetime.datetime.now()

        delta_t = (t - last_t).total_seconds()
        delta_v = abs(v - last_v) / 3.6  # m/s
        a = delta_v / delta_t

    print(v)
    f.write(str(v)+"\n")

    last_v = v
    last_t = t