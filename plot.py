import matplotlib.pyplot as plt

f = open('data', 'r')
lines = f.readlines()
f.close()

data = []
for line in lines:
    v = float(line.replace('\n', ''))
    data.append(v)

plt.plot(data)
plt.show()